spaced (1.2.0-201605+dfsg-4) unstable; urgency=medium

  * Add myself to Uploaders
    Closes: #1059714
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 05 Jan 2024 15:46:41 +0100

spaced (1.2.0-201605+dfsg-3) unstable; urgency=medium

  [ Shruti Sridhar ]
  * Team Upload.
  * Add test data
  * Add autopkgtests
  * Install README as docs

  [ Nilesh Patra ]
  * Bump Standards-Version to 4.5.1 (no changes needed)

 -- Shruti Sridhar <shruti.sridhar99@gmail.com>  Mon, 16 Aug 2021 16:10:19 +0530

spaced (1.2.0-201605+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Use secure URI in Homepage field.
  * Update renamed lintian tag names in lintian overrides.

 -- Andreas Tille <tille@debian.org>  Tue, 10 Nov 2020 17:21:03 +0100

spaced (1.2.0-201605+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream source
  * Fix get-orig-source script
  * Provide manpage in debian/spaced.1 instead of patch
  * Fake watch file
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.5
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Mon, 09 Jul 2018 19:12:45 +0200

spaced (1.0.2+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #815672)
  * Remove the precompiled binary via get-orig-source script
  * Add a manpage
  * Fix a problem if the compiler does not support OpenMP
  * Add autotools as proper build system

 -- Fabian Klötzl <kloetzl@evolbio.mpg.de>  Mon, 28 Mar 2016 19:41:33 +0200
